import subprocess
import datetime
import time
import os
import random
import string
import six.moves
from pathlib import Path
import time

current_milli_time = lambda: int(round(time.time() * 1000))

import jwt
import paho.mqtt.client as mqtt
from rta_sensor import RtaSensor

is_gpio_supported = True
try:
    pass
    #import RPi.GPIO as GPIO
    #raise RuntimeError('')
except RuntimeError:
    print('GPIO either not supported on this platform or you need to run script with sudo access')
    is_gpio_supported = False

class RtaClient:
    device_id_file_name = 'deviceName.rta'
    device_name_length = 16
    project_id = 'realtimesense'
    cloud_region = 'us-central1'
    registry_id = 'rtadevices'
    algorithum = 'RS256'
    ca_certs = 'roots.pem'
    mqtt_bridge_hostname = 'mqtt.googleapis.com'
    mqtt_bridge_port = 8883
    sub_topic = 'events'
    mqtt_topic = ''

    device_id = ''
    private_key_file = ''
    jwt_iat = datetime.datetime.utcnow()
    jwt_exp_mins = 20
    seconds_since_issue = -1
    client = None
    sensors = []
    keep_reading_sensors_in_loop = False
    milli_sample_rate = 1000
    last_data_poll_time = 0
    sensor_read_loop_start_time = 0

    def print1(self, string):
        print("[Device]: " + string)

    def setup(self):
        self.print1("(0/4) Begin device initialization.")
        self.print1("(1/4) Loading device ID.")
        self.create_or_load_device_id()
        self.print1("(2/4) Loading sensors.")
        self.load_sensors()
        self.mqtt_topic = '/devices/{}/{}'.format(self.device_id, self.sub_topic)
        self.print1("(3/4) Generating RSA keys.")
        self.generate_private_key_file()
        self.print1("(4/4) Done initializing.\n")

        current_directory = os.path.dirname(os.path.realpath(__file__))
        public_key_path = current_directory + os.sep + 'rsa_cert.pem'
        f = open(public_key_path, 'r')
        public_key = f.read()
        f.close()

        print("Copy and paste 'Device ID' and 'Public RSA key' into Real-Time Sense web app to register this device to a project!\n")

        print("Device ID:\n" + self.device_id + "\n")
        print("Public RSA key: \n" + public_key)

    def error(self, string):
        print("[ERROR]: " + string)

    def create_jwt(self):
        if self.private_key_file == '':
            self.generate_private_key_file()
        with open(self.private_key_file, 'r') as f:
            private_key = f.read()

        token = { 'iat' : datetime.datetime.utcnow(), 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60), 'aud': self.project_id }
        self.print1('Creating JWT using {} from private key file {}'.format(self.algorithum, self.private_key_file))  
        return jwt.encode(token, private_key, algorithm = self.algorithum)

    def generate_private_key_file(self):
        current_directory = os.path.dirname(os.path.realpath(__file__))
        self.private_key_file = current_directory + os.sep + 'rsa_private.pem'
        public_key_path = current_directory + os.sep + 'rsa_cert.pem'
        private_key_file = Path(self.private_key_file)
        public_key_file = Path(public_key_path)
        
        if (public_key_file.is_file() and private_key_file.is_file()):
            return
            
        subprocess.call(["openssl", "req", "-x509", "-newkey", "rsa:2048", "-keyout", self.private_key_file, "-nodes", "-out", public_key_path, "-subj", "/CN=unused"])

    def make_sure_client_has_valid_token(self):
        if (self.client == None):
            self.jwt_iat = datetime.datetime.utcnow()
            self.setup_client()
        self.seconds_since_issue = (datetime.datetime.utcnow() - self.jwt_iat).seconds

        if self.seconds_since_issue > 60 * self.jwt_exp_mins:
            self.print1('Refreshing token after {}s'.format(self.seconds_since_issue))
            self.client.loop_stop()
            self.jwt_iat = datetime.datetime.utcnow()
            self.setup_client()

    def publish_message(self, dataPayload):
        try:
            #self.print1('Publishing message: {}'.format(dataPayload))
            self.make_sure_client_has_valid_token()
            self.client.loop_start()
            self.client.publish(self.mqtt_topic, dataPayload, qos=1)
        except ValueError:
            self.client.loop_stop()
            print()
            self.error("There was an issue trying to initialize the MQTT IoT client")
            self.error("Possible solutions:")
            self.error("\t1. Make sure you have run the 'setup' command")
            self.error("\t2. Make sure your device was registered to a project on the Real Time Sense web app\n")

    def setup_client(self):
        try:
            client_id_string = 'projects/{}/locations/{}/registries/{}/devices/{}'.format(self.project_id, self.cloud_region, self.registry_id, self.device_id)
            self.print1("Setting up mqtt client for %s" % client_id_string)
            self.client = mqtt.Client(client_id = client_id_string)

            json_web_token = self.create_jwt()
            self.client.username_pw_set(username='', password=json_web_token)

            self.client.tls_set(ca_certs=self.ca_certs)

            self.client.on_connect = self.on_device_connected
            self.client.on_publish = self.on_publish_message_to_broker
            self.client.on_disconnect = self.on_paho_device_disconnected
            self.client.on_message = self.on_message_on_subscription
            self.client.connect(self.mqtt_bridge_hostname, self.mqtt_bridge_port)
            mqtt_config_topic = '/devices/{}/config'.format(self.device_id)
            self.client.subscribe(mqtt_config_topic, qos=1)
            self.client.loop_start()
            self.print1("mqtt client started")
        except ValueError:
            print()
            self.error("There was an issue trying to initialize the MQTT IoT client")
            self.error("Possible solutions:")
            self.error("\t1. Make sure you have run the 'setup' command")
            self.error("\t2. Make sure your device was registered to a project on the Real Time Sense web app\n")


    def error_str(self, rc):
        """Convert a Paho error to a human readable string."""
        return '{}: {}'.format(rc, mqtt.error_string(rc))

    def on_device_connected(self, unused_client, unused_userdata, unused_flags, rc):
        self.print1('on_connect {}'.format(mqtt.connack_string(rc)))


    def on_paho_device_disconnected(self, unused_client, unused_userdata, rc):
        self.print1('on_disconnect {}'.format(self.error_str(rc)))


    def on_publish_message_to_broker(self, unused_client, unused_userdata, unused_mid):
        self.print1('Message published to broker.')

    def on_message_on_subscription(self, unused_client, unused_userdata, message):
        payload = str(message.payload)
        self.print1('Received message \'{}\' on topic \'{}\' with Qos {}'.format(payload, message.topic, str(message.qos)))

    def create_or_load_device_id(self):
        current_directory = os.path.dirname(os.path.realpath(__file__))
        device_name_file_name = current_directory + os.sep + self.device_id_file_name
        device_name_path = Path(device_name_file_name)

        if (device_name_path.is_file()):
            f = open(device_name_file_name, "r") 
            self.device_id = f.read()
        else:
            self.device_id = self.create_random_device_name(self.device_name_length)
            f = open(device_name_file_name, "w")
            f.write(self.device_id)
            f.close()

    def create_random_device_name(self, length):
        char_options = string.ascii_letters + string.digits
        return ''.join(random.choice(char_options) for i in range(length))

    def save_sensors(self):
        current_directory = os.path.dirname(os.path.realpath(__file__))
        sensors_file_name = current_directory + '/deviceSensors.rta'
        sensors_file = open(sensors_file_name, "w")

        for sensor in self.sensors:
            newline = "\n"
            if (self.sensors[len(self.sensors) - 1] == sensor):
                newline = ""
            sensors_file.write("{} {}{}".format(sensor.name, sensor.unit, newline))

        sensors_file.close()

    def load_sensors(self):
        current_directory = os.path.dirname(os.path.realpath(__file__))
        sensors_file_name = current_directory + '/deviceSensors.rta'
        sensors_path = Path(sensors_file_name)

        if sensors_path.is_file():
            sensors_file = open(sensors_file_name, "r")
            for sensor_string in sensors_file.readlines():
                sensor_string_split = sensor_string.split(' ')
                if len(sensor_string_split) == 2:
                    sensor_name = sensor_string_split[0]
                    sensor_unit = sensor_string_split[1].strip()
                    self.sensors.append(RtaSensor(self, sensor_name, sensor_unit))
            sensors_file.close()

    def add_sensor(self, name, unit):
        for sensor in self.sensors:
            if (sensor.name == name):
                self.print1("Sensor with the name '{}' already exists on device".format(name))
                return False
        self.sensors.append(RtaSensor(self, name, unit))
        self.save_sensors()
        return True

    def remove_sensor(self, name):
        pass

    def send_all_sensor_data(self):
        current_time = current_milli_time()

        for sensor in self.sensors:
            sensor.sampleData()

        while (self.last_data_poll_time + self.milli_sample_rate < current_time):
            desired_sample_time = self.last_data_poll_time - (self.last_data_poll_time % self.milli_sample_rate)
            self.last_data_poll_time += self.milli_sample_rate
            data_payload = "{} \"deviceId\" : \"{}\",\n\"sensors\" : {}\n".format('{', self.device_id, '{')
            pretty_desired_time = desired_sample_time - self.sensor_read_loop_start_time
            for sensor in self.sensors:
                data_payload += sensor.getJsonForSensorData(desired_sample_time, pretty_desired_time)
                if (self.sensors[len(self.sensors) - 1] == sensor):
                    data_payload += "\n"
                else:
                    data_payload += ",\n"
            data_payload += "} }"         
            self.publish_message(data_payload)

    def start_reading_sensors_in_loop(self):
        if self.keep_reading_sensors_in_loop:
            return
        self.last_data_poll_time = current_milli_time()
        self.sensor_read_loop_start_time = self.last_data_poll_time - (self.last_data_poll_time % self.milli_sample_rate)
        six.moves._thread.start_new_thread(self.read_sensors_in_loop, ())

    def stop_reading_sensors_in_loop(self):
        self.keep_reading_sensors_in_loop = False

    def read_sensors_in_loop(self):
        self.keep_reading_sensors_in_loop = True
        while self.keep_reading_sensors_in_loop:
            self.send_all_sensor_data()
            time.sleep(self.milli_sample_rate / (3000.0))

    def clean_up(self):
        if (is_gpio_supported):
            GPIO.cleanup()
