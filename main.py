from rta_client import RtaClient
import six.moves
import command_handler

def main():
    print_welcome()

    rta_client = RtaClient()

    commands = {}
    commands['?'] = command_handler.help_command
    commands['help'] = command_handler.help_command
    commands['setup'] = command_handler.setup_command
    commands['test'] = command_handler.test_command
    commands['quit'] = command_handler.quit_command
    commands['q'] = command_handler.quit_command
    commands['sensor'] = command_handler.sensor_command
    commands['sensors'] = command_handler.sensor_command

    do_command_loop = True
    while do_command_loop:
        args = six.moves.input("[RTA] >> ").split(' ')

        if len(args) < 1:
            continue

        command = args[0]
        if command in commands:
            result = commands[command](rta_client, args[1:])
            if not result is None:
                do_command_loop = False
        else:
            print("Command '{}' not found. Type '?' for help.".format(command))

def print_welcome():
    print("")
    print("{:->80}".format(""))
    print("REAL TIME SENSE v0.0.alpha       ")
    print("Device Software for Raspberry Pi ")
    print("                                 ")
    print("Type {:<10} to {}".format("'{}'".format("setup"), "setup device"))
    print("Type {:<10} to {}".format("'{}'".format("?"), "get help"))
    print("{:->80}".format(""))

if __name__ == '__main__':
    main()
